
#include "WndProcHandlers.h" // interface module
#include <stdio.h> // for spec data stdio
#include <windef.h>
#include "AddModule.h" //
#include <math.h> // for math functions
#include <unistd.h>

static int SAVED_VPOS = 0;
static int SAVED_HPOS = 0;

HSCROLL_FLAG_WMHADLER = 0;
VSCROLL_FLAG_WMHADLER = 0;

static void caret_pos_return(HWND hwnd,
							unsigned* cur_string_num,
							int cur_pos_in_string,
							int* vert_pos, int* horz_pos,
							//int saved_v_pos, int saved_h_pos,
							int* xCaret, int* yCaret,
							unsigned num_lines,
							int screen_high_symb,
							int screen_width_symb)
{
	/* return in window-area */
	if(*cur_string_num >= *vert_pos
		&& *cur_string_num < *vert_pos + screen_high_symb)
	{
		*yCaret = *cur_string_num - *vert_pos;
	}
	else
	{
		//SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, 3);
		VSCROLL_FLAG_WMHADLER = 1;
		//SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, *cur_string_num);
		SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, SAVED_VPOS);
		VSCROLL_FLAG_WMHADLER = 0;
		//*yCaret = *cur_string_num - *vert_pos;
	}
	if(cur_pos_in_string >= *horz_pos
		&& cur_pos_in_string <= *horz_pos + screen_width_symb)
	{
		*xCaret = cur_pos_in_string - *horz_pos;
	}
	else
	{
//		SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION,
//												cur_pos_in_string - screen_width_symb);
		HSCROLL_FLAG_WMHADLER = 1;
		SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION,
												SAVED_HPOS);
		HSCROLL_FLAG_WMHADLER = 0;
		// *xCaret = 0;//cur_pos_in_string - *horz_pos - 1;
	}

}


void RecountStartIndexCaret(unsigned* offsets, unsigned offsets_size,
                            unsigned screen_len_symb, unsigned screen_high_symb,
                            unsigned* in_offset_pos,
                            unsigned prev_off,
                            unsigned* str_num,
                            int* str_pos, HWND hwnd,
                            int* xCaret, int* yCaret,
                            int* vert_pos, int* horz_pos)
{
	unsigned prev_str_num = *str_num;
	unsigned prev_str_pos = *str_pos;
	unsigned new_str_num = 0;
	unsigned new_str_pos = 0;
	unsigned sum = 0;
	unsigned i = 0;

    while(i < offsets_size)
    {
        i++;
        sum += (offsets[i] - offsets[i - 1]);
        //if(offsets[i] >= prev_off)
        if(offsets[i] >= *in_offset_pos)
        {
            break;
        }
    }
    *str_num = i;
    //*str_pos = offsets[*str_num] - sum;
    sum -= (offsets[i] - offsets[i - 1]);
    *str_pos = *in_offset_pos - sum;
    *str_num -= 1;


    //*horz_pos = imax(*str_pos - screen_len_symb, 0);
    VSCROLL_FLAG_WMHADLER = 1;
    //SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, 0L);
    SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, *str_num);
    VSCROLL_FLAG_WMHADLER = 0;

    HSCROLL_FLAG_WMHADLER = 1;
    SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION, *str_pos);
    HSCROLL_FLAG_WMHADLER = 0;
    //SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION, 0L);
    //*yCaret = imin((offsets_size - screen_high_symb + 1), *str_num);

    printf("    vert_pos from_rec = %i \n", *vert_pos);
    *yCaret = *str_num - *vert_pos;
    printf("    horz_pos from_rec = %i \n", *vert_pos);
    *xCaret = *str_pos - *horz_pos;

    printf("*yCaret from rec = %i \n", *yCaret);
    printf("*xCaret from rec = %i \n", *xCaret);

  //  *yCaret = imin(*str_num, offsets_size - *str_num + 1);


    //*yCaret = *str_num - *vert_pos;



    SAVED_HPOS = *str_pos;
    SAVED_VPOS = *str_num;

}


void wm_keydown_handler(HWND hwnd,
						WPARAM wParam, LPARAM lParam,
						int* xCaret, int* yCaret,
						int cxChar, int cyChar,
						int* vert_pos, int* horz_pos,
						BOOL* realloc_offset_flag,
						unsigned* offsets,
						unsigned offset_size,
						unsigned* in_offset_pos,
						unsigned* cur_string_num, // current string num in the text
						unsigned screen_len_symb,
						unsigned screen_high_symb,
						unsigned buf_len_symb)
{
	int prev_string_len = 0;
	int cur_string_len = 0;
	int pos_in_str = 0;
	int next_string_len;

	if((int)*in_offset_pos < 0)
    {
        printf("position error \n");
        SendMessage(hwnd, WM_DESTROY, 0, 0L);
    }

	cur_string_len = offsets[*cur_string_num + 1] - offsets[*cur_string_num];

	caret_pos_return(hwnd,
					 cur_string_num,
					(int)*in_offset_pos - (int)offsets[*cur_string_num],
					vert_pos, horz_pos,
					xCaret, yCaret,
					offset_size, screen_high_symb, screen_len_symb);

	switch(wParam)
	{
		case VK_RIGHT:
		{
			printf("	VK_RIGHT \n"); // spec_data
			/* if it is last symbol in text - we can't move */
			if(*in_offset_pos == buf_len_symb)
			{
				printf("	last symbol in text \n");
				break;
			}
			pos_in_str = (int)*in_offset_pos - (int)offsets[*cur_string_num];
			*in_offset_pos += 1;
			pos_in_str += 1;
			/* if it's in window, not on right border */
			if(pos_in_str < *horz_pos + screen_len_symb)
			{
                if(pos_in_str < cur_string_len)
				{
					*xCaret += 1;
					break;
				}
				/* extra? */
				if(*cur_string_num == offset_size)
				{
					printf("	last symbol in text \n");
					break;
				}
				if(*cur_string_num >= *vert_pos + screen_high_symb - 1)
				//if(*yCaret >= *vert_pos + screen_high_symb - 1)
				{
					SendMessage(hwnd, WM_VSCROLL, SB_LINEDOWN, 0L);
					SendMessage(hwnd, WM_HSCROLL, SB_BOTTOM, 0L);
					*xCaret = 0;
					*cur_string_num += 1;
					break;
				}
				SendMessage(hwnd, WM_HSCROLL, SB_BOTTOM, 0L);
				*xCaret = 0;
				*yCaret += 1;
				*cur_string_num += 1;
				break;
			}
			/* if symbol is on right border */
			else
			{
				/* if not last in string */
				if(pos_in_str < cur_string_len)
				{
					SendMessage(hwnd, WM_HSCROLL, SB_LINERIGHT, 0L);
					break;
				}
				/* extra? */
				if(*cur_string_num == offset_size)
				{
					printf("	last symbol in text \n");
					break;
				}
				if(*cur_string_num >= *vert_pos + screen_high_symb - 1)
				//if(*yCaret >= *vert_pos + screen_high_symb - 1)
				{
					SendMessage(hwnd, WM_VSCROLL, SB_LINEDOWN, 0L);
					SendMessage(hwnd, WM_HSCROLL, SB_BOTTOM, 0L);
					*xCaret = 0;
					*cur_string_num += 1;
					break;
				}
				SendMessage(hwnd, WM_HSCROLL, SB_BOTTOM, 0L);
				*xCaret = 0;
				*yCaret += 1;
				*cur_string_num += 1;
			}
			break;
		}
		case VK_LEFT:
		{
			printf("	VK_LEFT \n"); // spec_data
			/* if it is first symbol in text - we can't move */
			if(*in_offset_pos == 0)
			{
				printf("	1st symbol in text \n");
				break;
			}
			pos_in_str = (int)*in_offset_pos - (int)offsets[*cur_string_num];
			*in_offset_pos -= 1;
			pos_in_str -= 1;
			/* if it's in window, not on left border */
			if(pos_in_str >= *horz_pos && pos_in_str >= 0)
			{
				*xCaret -= 1;
				break;
			}
			/* if it's on left border */
			//if(pos_in_str == *horz_pos)
			if(pos_in_str < *horz_pos)
			{
				prev_string_len = offsets[*cur_string_num] - offsets[*cur_string_num - 1];
				/* the same pos_in_str > 0 */
				if(*horz_pos > 0)
				{
					SendMessage(hwnd, WM_HSCROLL, SB_LINELEFT, 0L);
					break;
				}
				/* if it's not first string in window*/
				if(*cur_string_num > *vert_pos)
				{
					HSCROLL_FLAG_WMHADLER = 1;
					SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION,
									imax(0, prev_string_len - screen_len_symb)
									/* prev_string_len - screen_len_symb*/ );
					//printf("plen = %i , scr_len = %i \n", prev_string_len, screen_len_symb);
					HSCROLL_FLAG_WMHADLER = 0;
					*cur_string_num -= 1;
					*yCaret -= 1;
					*xCaret = imin(screen_len_symb, prev_string_len) - 1;
					break;
				}
				HSCROLL_FLAG_WMHADLER = 1;
				SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION,
												prev_string_len - screen_len_symb);
				HSCROLL_FLAG_WMHADLER = 0;
				SendMessage(hwnd, WM_VSCROLL, SB_LINEUP, 0L);
				*cur_string_num -= 1;
				//*yCaret -= 1;
				*xCaret = imin(screen_len_symb, prev_string_len) - 1;
				break;

			}
			break;
		}
		case VK_DOWN:
		{
			printf("	VK_DOWN \n"); // spec_data
			if(*cur_string_num == offset_size - 1)
			{
				printf("	last string in text \n"); // spec data
				break;
			}
			if(*cur_string_num == offset_size - 2)
			{
				printf("	last but one string in text \n"); // spec data
				next_string_len = 1;
				//next_string_len = 0;
				//next_string_len = buf_len_symb - offsets[offset_size];
			}
			else
			{
				printf("	classic string \n"); //spec data
				next_string_len = offsets[*cur_string_num + 2]- offsets[*cur_string_num + 1];
			}
			printf("	next_string_len = %i \n", next_string_len);
			pos_in_str = (int)*in_offset_pos - (int)offsets[*cur_string_num];
			/* if string not in window down-border */
			if(*cur_string_num < *vert_pos + screen_high_symb - 1)
			{
			/* but if on border */
				if(pos_in_str < next_string_len)
				{
					*yCaret += 1;
					*cur_string_num += 1;
					*in_offset_pos += cur_string_len;
					break;
				}
				*yCaret += 1;
				*cur_string_num += 1;
				*in_offset_pos += next_string_len + (cur_string_len - pos_in_str) - 1;
				/* pos in new string */
				pos_in_str = next_string_len - 1;

				printf("%i, %i, max = %i \n", 0,
						pos_in_str - screen_len_symb,
						imax(0, pos_in_str - screen_len_symb));
				HSCROLL_FLAG_WMHADLER = 1;
				SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION, imax(0, pos_in_str - screen_len_symb)); // ???
				HSCROLL_FLAG_WMHADLER = 0;
				*xCaret = pos_in_str - imax(0, pos_in_str - screen_len_symb);
				//*xCaret = pos_in_str - (cur_string_len - next_string_len);
				printf("	*xCaret = %i \n", *xCaret); // spec data
				printf("	horz_pos (fromDOWN) = %i \n", *horz_pos); //spec data
				break;
			}
			if(pos_in_str < next_string_len)
			{
				//*yCaret += 1;
				*cur_string_num += 1;
				*in_offset_pos += cur_string_len;
				SendMessage(hwnd, WM_VSCROLL, SB_LINEDOWN, 0L);
				break;
			}

			*cur_string_num += 1;
			*in_offset_pos += next_string_len + (cur_string_len - pos_in_str) - 1;
			/* pos in new string */
			if(next_string_len > 0)
				pos_in_str = next_string_len - 1;
			else
				pos_in_str = 0;

			printf("%i, %i, max = %i \n", 0,
					pos_in_str - screen_len_symb,
					imax(0, pos_in_str - screen_len_symb));
			HSCROLL_FLAG_WMHADLER = 1;
			SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION, imax(0, pos_in_str - screen_len_symb)); // ???
			HSCROLL_FLAG_WMHADLER = 0;
			SendMessage(hwnd, WM_VSCROLL, SB_LINEDOWN, 0L);
			*xCaret = pos_in_str - imax(0, pos_in_str - screen_len_symb);
			//*xCaret = pos_in_str - (cur_string_len - next_string_len);

			break;
		}
		case VK_UP:
		{
			printf("	VK_UP \n"); // spec_data
			if(*cur_string_num == 0)
			{
				printf("	first string in text \n"); // spec data
				break;
			}
			prev_string_len = offsets[*cur_string_num] - offsets[*cur_string_num - 1];
			pos_in_str = (int)*in_offset_pos - (int)offsets[*cur_string_num];
			/* if it's not on top border */
			if(*cur_string_num > *vert_pos)
			{
				if(pos_in_str < prev_string_len)
				{
					*yCaret -= 1;
					*cur_string_num -= 1;
					*in_offset_pos -= prev_string_len;
					break;
				}
				*yCaret -= 1;
				*in_offset_pos -= (pos_in_str + 1);
				*cur_string_num -= 1;
				/* new pos in str */
				pos_in_str = prev_string_len - 1;
				HSCROLL_FLAG_WMHADLER = 1;
				SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION,
								imax(0, pos_in_str - screen_len_symb));
								HSCROLL_FLAG_WMHADLER = 0;
				*xCaret = pos_in_str - imax(0, pos_in_str - screen_len_symb);
				break;
			}
			if(pos_in_str < prev_string_len)
			{
				//*yCaret -= 1;
				*cur_string_num -= 1;
				*in_offset_pos -= prev_string_len;
				SendMessage(hwnd, WM_VSCROLL, SB_LINEUP, 0L);
				break;
			}
			//*yCaret -= 1;
			*in_offset_pos -= (pos_in_str + 1);
			*cur_string_num -= 1;
			/* new pos in str */
			pos_in_str = prev_string_len - 1;
			SendMessage(hwnd, WM_HSCROLL, SB_THUMBPOSITION,
							imax(0, pos_in_str - screen_len_symb));
			SendMessage(hwnd, WM_VSCROLL, SB_LINEUP, 0L);
			*xCaret = pos_in_str - imax(0, pos_in_str - screen_len_symb);
			break;
		}
		case VK_ESCAPE:
		{
			printf("	VK_ESCAPE \n");
			SendMessage(hwnd, WM_DESTROY, 0, 0L);
			return;
		}
		default:
		{
			printf("	DEFAULT - unknown key \n"); // spec_data
			break;
		}
	}
	printf(" from WMHandler data \n"); // spec data
	printf("	*xCaret = %i \n", *xCaret); // spec data
	printf("	*yCaret = %i \n", *yCaret); // spec data
	printf("	*cur_string_num = %i \n", *cur_string_num); //spec data
	printf("	horz_pos = %i \n", *horz_pos); //spec data
	*realloc_offset_flag = FALSE;
	SendMessage(hwnd, WM_SIZE, 0, 0L);
	*realloc_offset_flag = TRUE;
	SAVED_HPOS = *horz_pos;
	SAVED_VPOS = *vert_pos;
}

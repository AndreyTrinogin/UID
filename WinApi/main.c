#if defined(UNICODE) && !defined(_UNICODE)
    #define _UNICODE
#elif defined(_UNICODE) && !defined(UNICODE)
    #define UNICODE
#endif

#include <stdio.h> /* stdio for debug */
#include <tchar.h> /* universalc char */
#include <windows.h> /* windows api */
#include <locale.h> /* for russian lng */
#include "WndProcHandlers.h" /* for my WM_* cases handlers */
#include "TextHandler.h" /* for creating offsets and etc with text */
#include "AddModule.h" /* additional functions */
#include "Menu.h" /* for my menu */
#include "ScrollBar.h" /* my functions for scroll bars */

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
TCHAR szClassName[ ] = _T("Trinogin Andrey App");


int WINAPI WinMain (HINSTANCE hThisInstance, HINSTANCE hPrevInstance,
                     LPSTR lpszArgument, int nCmdShow)
{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the window class */
    setlocale(LC_ALL, "Russian"); /* For supporting russian */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS | CS_CLASSDC | CS_VREDRAW | CS_HREDRAW; /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = "Menu";                /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    wincl.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           _T("Trinogin Andrey App"),       /* Title Text */
           WS_OVERLAPPEDWINDOW | WS_VSCROLL | WS_HSCROLL, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           400,                 /* The programs width */
           300,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           lpszArgument         /* Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nCmdShow);
    UpdateWindow(hwnd);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}

static OPENFILENAME ofn;

void InitializePopFile(HWND hwnd)
{
    static char szFilter[] = "Text\0*.txt\0\0";
    ofn.lpstrFilter = szFilter;
    ofn.lStructSize = sizeof(OPENFILENAME); /* size of structure */
    ofn.hInstance = NULL; /* It is taken into account only if special flags are set. */
    ofn.lpstrCustomFilter = NULL; /* A pointer to a static buffer containing a pair of null-terminated strings to define a user pattern */
    ofn.lpstrDefExt = "txt";
    ofn.nMaxFile = MAX_PATH; /* determines the size of the buffer to which a separate parameter lpstrFile */
    ofn.lpstrInitialDir = NULL; /* starting directory */
    ofn.lpstrFile = NULL; /* Set in Open and Close functions, pointer to the buffer in which the full, given path, file name is specified */
}

BOOL FileOpenDlg(OPENFILENAME *ofn, HWND hwnd, PSTR pstrFileName, PSTR pstrTitleName)
{
    ofn->hwndOwner = NULL; /* identifies the window that is "owner" */
    ofn->lpstrFile = pstrFileName;
    ofn->lpstrFileTitle = pstrTitleName;
    ofn->lpstrTitle = TEXT("Please Select a file");
    ofn->Flags = OFN_HIDEREADONLY | OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    return GetOpenFileName(ofn);
}



/*  This function is called by the Windows function DispatchMessage()  */
LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    static char* file_name = NULL; // open file name

    static const char ab_prog [] = "Trinogin Andrey App, 33631/4, 2018";
    static const char ab_str[] = "About program";

    static str_t buf = {NULL, 0}; // my buffer, string +size of string (in symbols)

    static unsigned num_lines = 0; // number of strings
    static unsigned max_string_len = 0; // length of biggest string in file
    static unsigned* offsets = NULL; // array of offsets

    static char szFileName[MAX_PATH]; // for file opening
    static char szTitleName[MAX_PATH];

    static unsigned cxChar = 0; // sizes for symbol
    static unsigned cyChar = 0;

    static unsigned cxClient = 0; // sizes for clients window
    static unsigned cyClient = 0;

    static int width = 0;
    static int high = 0;

    static int xCaret = 0; // for caret positinoning
    static int yCaret = 0; //
    static unsigned off_pos = 0; // position of caret in offset
    static unsigned cur_string_num = 0; // number of current string for caret
    static int cur_str_pos = 0;


    static BOOL realloc_offset_flag = TRUE;
    static BOOL need_recount = FALSE; // bool flag for recount start string index
    static BOOL need_page_setup_once = TRUE;

    static int vert_pos = 0;
    static int horz_pos = 0;

    /** this 2 indexes, paint bef and paint end is number of string */
    static unsigned paint_beg = 0; // current start index for painting
    static unsigned paint_end = 0; // current end index for painting

    /** this is offset from start, this is NOT STRING NUMBER */
    static unsigned prev_beg = 0; // previously start index

    static unsigned idm_mode; // scroll or wrap mode
    static unsigned car_rec = 0;

    HDC hdc; // handler device context
    CREATESTRUCT *ms = NULL;
    PAINTSTRUCT ps; // paintstruct
    TEXTMETRIC tm; // text metrics
    SCROLLINFO si; // for scrollbars, 32bit
	static HMENU hMenu; // my menu

    switch (message) /* handler the messages */
    {
        case WM_CREATE:
        {
            printf("WM_CREATE \n"); // spec_data
            ms = (CREATESTRUCT*)lParam;
            hdc = GetDC(hwnd);
            SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));
            GetTextMetrics(hdc, &tm);
            cxChar = tm.tmAveCharWidth; /* average width */
            cyChar = tm.tmHeight + tm.tmExternalLeading; /* height + line spacing */
			hMenu = GetMenu(hwnd);
			ReleaseDC(hwnd, hdc);

            idm_mode = IDM_MODE_SCROLL; // default mode is scroll
            CheckMenuItem(hMenu, idm_mode, MF_CHECKED);
			EnableMenuItem(hMenu, idm_mode, MF_GRAYED);

            file_name = (char*)ms->lpCreateParams;
            if (file_name != NULL)
            {
                buf = ReadFileToStrT(file_name);
                num_lines = CalcNumLines(buf, &max_string_len);
                InitializePopFile(hwnd);
                printf("    file name = %s \n", file_name);
                printf("    number of lines in file = %i \n", num_lines);
            }

            xCaret = 0;
            yCaret = 0;
            off_pos = 0;

            return 0;
        }
        case WM_PAINT:
        {

            int x;
            int y;
            printf("WM_PAINT \n"); // spec_data

            hdc = BeginPaint(hwnd, &ps);

            si.cbSize = sizeof(si);
            si.fMask = SIF_POS;

            GetScrollInfo(hwnd, SB_VERT, &si);
            vert_pos = si.nPos;

            GetScrollInfo(hwnd, SB_HORZ, &si);
            horz_pos = si.nPos;

            paint_beg = max(0, vert_pos + ps.rcPaint.top / cyChar);
            paint_end = min(num_lines, vert_pos + ps.rcPaint.bottom / cyChar);

            printf("    paint_beg = %i\n", paint_beg); // spec_data
            printf("    paint_end = %i\n", paint_end); // spec_data
            printf("    vert_pos = %i\n", vert_pos);
            printf("    horz_pos = %i\n", horz_pos);

            x = cxChar * ( -horz_pos);
            for (unsigned i = paint_beg; i <= paint_end; i++)
            {
                y = cyChar * ( -vert_pos + i);
                if(i >= num_lines)
                    break;
                TextOut(hdc, x, y, buf.str + offsets[i], offsets[i + 1] - offsets[i]);
            }

            EndPaint(hwnd, &ps);

            if(offsets != NULL)
            {
                prev_beg = offsets[paint_beg];
                printf("    prev_beg = %i \n", prev_beg); // spec_data
            }

            return 0;
        }
        case WM_SIZE:
        {
            cxClient = LOWORD(lParam);
            cyClient = HIWORD(lParam);

            if(cxClient != 0)
                width = cxClient / cxChar;
            if(cyClient != 0)
                high = cyClient / cyChar;

            if(cxClient != 0 && cyClient != 0)
                    need_recount = TRUE;


            printf("WM_SIZE \n"); 					  // spec_data
            printf("    cxClient = %i \n", cxClient); // spec_data
            printf("    cyClient = %i \n", cyClient); // spec_data

            if(offsets != NULL && realloc_offset_flag == TRUE)
            {
                printf("    Free offsets \n");
                free(offsets);
                offsets = NULL;
            }

            switch(idm_mode)
            {
                case IDM_MODE_WRAP:
                    printf("    IDM_MODE_WRAP RESIZE \n"); // spec_data
                    si = i_SetScrollRange_32(hwnd, SB_HORZ, si, TRUE, 0, 0);
                    //si = i_SetScrollPage_32(hwnd, SB_HORZ, si, FALSE, cxClient / cxChar);
                    if(realloc_offset_flag == TRUE)
                        offsets = CreatePointerWr(buf, &num_lines, cxClient / cxChar);
                    break;

                case IDM_MODE_SCROLL:
                    printf("    IDM_MODE_SCROLL RESIZE \n"); // spec_data
                    si = i_SetScrollRange_32(hwnd, SB_HORZ, si, TRUE, 0, max_string_len + 2);
                    //si = i_SetScrollPage_32(hwnd, SB_HORZ, si, FALSE, cxClient / cxChar);
                    if(realloc_offset_flag == TRUE)
                        offsets = CreatePointerSc(buf, &num_lines);
                    break;

                default:
                    break;
            }

            si = i_SetScrollRange_32(hwnd, SB_VERT, si, TRUE, 0, num_lines - 1);
            if(need_page_setup_once)
            {
                si = i_SetScrollPage_32(hwnd, SB_VERT, si, TRUE, cyClient / cyChar);

            }
            if (need_recount)
            {
                vert_pos = RecountStartIndex(offsets, num_lines, prev_beg + 1);
                cur_str_pos = (int)off_pos - (int)offsets[cur_string_num];
                //if(need_page_setup_once != TRUE)
               // {

              //  }
                need_page_setup_once = FALSE;
                printf("after caret recount \n");
                printf("cur_string_num = %i,\ncur_str_pos = %i \n", cur_string_num, cur_str_pos);
                //si = i_SetScrollRange_32(hwnd, SB_VERT, si, TRUE, 0, num_lines);
                si = i_SetScrollPos_32(hwnd, SB_VERT, si, TRUE, vert_pos);
                //if(vert_pos != 0) // start of program
               // {
                    SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, 1);
               // }
               // SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, 2);
                printf(" from need recount vert_pos == %i \n", vert_pos);
    //            SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, 2);
      //          printf(" from need recount vert_pos == %i \n", vert_pos);
//                si = i_SetScrollPos_32(hwnd, SB_VERT, si, TRUE, vert_pos);
//                SendMessage(hwnd, WM_VSCROLL, SB_THUMBTRACK, 2);
                //need_recount = FALSE;
            }


            if(need_recount != TRUE)
                if(hwnd == GetFocus())
                {
                    SetCaretPos(xCaret * cxChar, yCaret * cyChar);
                }


            need_recount = FALSE;
            return 0;
        }
        case WM_SETFOCUS: // create and show caret
        {
            printf("WM_SETFOCUS \n");
            CreateCaret(hwnd, NULL, cxChar, cyChar);
            SetCaretPos(xCaret * cxChar, yCaret * cyChar);
            ShowCaret(hwnd);
            return 0;
        }
        case WM_KILLFOCUS: // hide and destroy the caret
        {
            printf("WM_KILLFOCUS \n");
            HideCaret(hwnd);
            DestroyCaret();
            return 0;
        }
        case WM_COMMAND:
        {
            int prev_mode = idm_mode;
            printf("WM_COMMAND \n"); // spec_data
            switch(LOWORD(wParam))
            {
                case IDM_MODE_SCROLL:
                    printf("    IDM_MODE_SCROLL \n"); // spec_data
                    //break;
                case IDM_MODE_WRAP:
                    printf("    IDM_MODE_WRAP \n"); // spec_data
                    HideCaret(hwnd);
                    DestroyCaret();
                    CheckMenuItem(hMenu, idm_mode, MF_UNCHECKED);
                    EnableMenuItem(hMenu, idm_mode, MF_ENABLED);
                    idm_mode = LOWORD(wParam);
                    CheckMenuItem(hMenu, idm_mode, MF_CHECKED);
                    EnableMenuItem(hMenu, idm_mode, MF_GRAYED);
                    need_recount = TRUE;
                    SendMessage(hwnd, WM_SIZE, 0, cyClient << 16 | cxClient);
                    InvalidateRect(hwnd, NULL, TRUE);
                    need_page_setup_once = TRUE;

                    if(prev_mode != idm_mode)
                    {
                        car_rec = 1;
                    }
//                    CreateCaret(hwnd, NULL, cxChar, cyChar);
//                    SetCaretPos(xCaret * cxChar, yCaret * cyChar);
//                    ShowCaret(hwnd);
                    break;
                case IDM_FILE_OPEN:
                {
                    printf("    IDM_FILE_OPEN \n"); // spec_data
                    if(FileOpenDlg(&ofn, hwnd, szFileName, szTitleName))
                    {
                        if(buf.str != NULL)
                        {
                            free(buf.str);
                            buf.str = NULL;
                            buf.len = 0;
                        }
                        buf = ReadFileToStrT(szFileName);
                        num_lines = CalcNumLines(buf, &max_string_len);
                        CheckMenuItem(hMenu, idm_mode, MF_UNCHECKED);
                        EnableMenuItem(hMenu, idm_mode, MF_ENABLED);
                        idm_mode = IDM_MODE_SCROLL;
                        CheckMenuItem(hMenu, idm_mode, MF_CHECKED);
                        EnableMenuItem(hMenu, idm_mode, MF_GRAYED);
                        printf("%i and %i \n", cyClient, cxClient);
                        printf(" = %i \n", cyClient << 16 | cxClient);
                        SendMessage(hwnd, WM_SIZE, 0, cyClient << 16 | cxClient);

                        si = i_SetScrollPos_32(hwnd, SB_VERT, si, TRUE, 0); // new nt
                        si = i_SetScrollPos_32(hwnd, SB_HORZ, si, TRUE, 0); // new nt

                        xCaret = 0;
                        yCaret = 0;
                        off_pos = 0;
                        cur_string_num = 0;

                        InvalidateRect(hwnd, NULL, TRUE);
                    }
                    break;
                }
                case IDM_FILE_EXIT:
                    printf("    IDM_FILE_EXIT \n"); // spec_data
                    SendMessage(hwnd, WM_CLOSE, 0, 0L);
                    break;

                case IDM_ABOUT:
                    printf("    IDM_ABOUT \n"); // spec_data
                    MessageBox(hwnd, ab_prog, ab_str, MB_OK);
                    break;
            }
            return 0;
        }
        case WM_KEYDOWN:
        {
            printf("WM_KEYDOWN \n"); // spec_data
            printf("    before in_offset_pos = %i, buf[%i] = %c \n",
                   off_pos, off_pos, buf.str[off_pos]);
            if(car_rec == 1)
            {
                RecountStartIndexCaret(offsets, num_lines,
                                        width, high,
                                        &off_pos, offsets[cur_string_num],
                                        &cur_string_num,
                                        &cur_str_pos, hwnd,
                                        &xCaret, &yCaret,
                                        &vert_pos, &horz_pos);
                car_rec = 0;
                CreateCaret(hwnd, NULL, cxChar, cyChar);
                SetCaretPos(xCaret * cxChar, yCaret * cyChar);
                ShowCaret(hwnd);
            }

            wm_keydown_handler(hwnd, wParam, lParam,
                               &xCaret, &yCaret,
                               cxChar, cyChar,
                               &vert_pos, &horz_pos,
                               &realloc_offset_flag,
                               offsets, num_lines,
                               &off_pos,
                               //&vert_pos, //
                               &cur_string_num,
                               width, high,
                               buf.len);
            if(buf.str != NULL)
                printf("    after in_offset_pos = %i, buf[%i] = %c \n",
                   off_pos, off_pos, buf.str[off_pos]);
            return 0;
        }
        case WM_VSCROLL:
        {
            si.cbSize = sizeof (si);
            si.fMask = SIF_ALL;
            GetScrollInfo (hwnd, SB_VERT, &si);
//            if(lParam != 2) // new
//                vert_pos = si.nPos;

            printf("WM_VSCROLL \n"); // spec_data
            switch (LOWORD(wParam))
            {
                case SB_TOP: /* maximum scroll bar */
                    si.nPos = si.nMin;
                    break;
                case SB_BOTTOM: /* minimum scroll bar */
                    si.nPos = si.nMax;
                    break;
                case SB_LINEUP: /* slider position up one position */
                    si.nPos -= 1;
                    break;
                case SB_LINEDOWN: /* slider position down one position */
                    si.nPos += 1;
                    break;
                case SB_PAGEUP: /* slider one page up */
                    si.nPos -= si.nPage;
                    break;
                case SB_PAGEDOWN: /* slider one page down */
                    si.nPos += si.nPage;
                    break;
                case SB_THUMBTRACK: /* while moving the slider, the mouse button is held down */
                    si.nPos = si.nTrackPos;
                    if(VSCROLL_FLAG_WMHADLER == 1)
                        si.nPos = lParam;
                    //if(lParam == 3)
                        //si.nPos = cur_string_num;//abs((int)vert_pos - (int)cur_string_num);
                    //else
                    break;
/*                    if(lParam == 3)
                        si.nPos = cur_string_num;//abs((int)vert_pos - (int)cur_string_num);
                        //si.nTrackPos = v//abs((int)vert_pos - (int)cur_string_num);
                    else
                        si.nPos = si.nTrackPos;
                    break; */
                default:
                    break;
            }

            si.fMask = SIF_POS;
            SetScrollInfo(hwnd, SB_VERT, &si, TRUE);
            GetScrollInfo(hwnd, SB_VERT, &si);

            if(si.nPos != vert_pos)
            {
                ScrollWindow(hwnd, 0, cyChar * (vert_pos - si.nPos), NULL, NULL);
                InvalidateRect(hwnd, NULL, TRUE);
            }
            printf(" from vscroll\n ");
            printf("   vert_pos =  %i \n", vert_pos); // spec_data
            printf("   si.nTrackPos =  %i \n", si.nTrackPos); // spec_data
            printf("   si.nMax =  %i \n", si.nMax); // spec_data
            printf("   si.nMin =  %i \n", si.nMin); // spec_data
            GetScrollInfo(hwnd, SB_VERT, &si);
            vert_pos = si.nPos;
            printf("   vert_pos after scr =  %i \n", vert_pos); // spec_data
            return 0;
        }
        case WM_HSCROLL:
        {
            si.cbSize = sizeof(si);
            si.fMask = SIF_ALL;
            // Save the position for comparison later on
            GetScrollInfo (hwnd, SB_HORZ, &si);
            horz_pos = si.nPos;

            printf("WM_HSCROLL\n"); // spec_data
            switch (LOWORD(wParam))
            {
                case SB_LINELEFT:
                    si.nPos -= 1;
                    break;
                case SB_LINERIGHT:
                    si.nPos += 1;
                    break;
                case SB_PAGELEFT:
                    si.nPos -= si.nPage;
                    break;
                case SB_PAGERIGHT:
                    si.nPos += si.nPage ;
                    break;
                case SB_THUMBPOSITION:
                    //if(si.nTrackPos != 0)
                        /* other flag*/
                        si.nPos = si.nTrackPos; // not tested ok
                    //if(lParam != -1 )
                    //    si.nPos = lParam;
                    if(HSCROLL_FLAG_WMHADLER == 1)
                        si.nPos = lParam;
                    break;
                case SB_BOTTOM:
                    si.nPos = si.nMin;
                    break;
                case SB_TOP:
                    si.nPos = si.nMax;
                    break;
                default:
                    break;
            }

            si.fMask = SIF_POS ;
            SetScrollInfo (hwnd, SB_HORZ, &si, TRUE);
            GetScrollInfo (hwnd, SB_HORZ, &si) ;
            // If the position has changed, scroll the window
            if(si.nPos != horz_pos)
            {
                ScrollWindow (hwnd, cxChar * (horz_pos - si.nPos), 0, NULL, NULL) ;
                InvalidateRect(hwnd, NULL, TRUE);
            }
            //horz_pos = si.nPos;
            //printf("    horz_pos = %i \n", horz_pos); // spec_data

            GetScrollInfo(hwnd, SB_HORZ, &si);
            horz_pos = si.nPos;
            printf("   horz_pos after scroll =  %i \n", horz_pos); // spec_data

            return 0;
        }
        case WM_DESTROY:
        {
            if(!offsets)
            {
                free(offsets);
                offsets = NULL;
            }
            if(!ms)
            {
                free(ms);
                ms = NULL;
            }
            if(buf.str != NULL)
            {
                free(buf.str);
                buf.str = NULL;
            }
            PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
            break;
        }
        default:                      /* for messages that we don't deal with */
            return DefWindowProc (hwnd, message, wParam, lParam);
    }

    return 0;
}

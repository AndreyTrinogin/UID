#ifndef SCROLLBAR_H_INCLUDED
#define SCROLLBAR_H_INCLUDED

#include <windows.h> /* for windows api */

/**
	hwnd[IN] - app window handler
	id[IN] - scrollbar id
	si[IN] - SCROLLINFO struct for actions
	rp[IN] - value for request repaiting
*/
SCROLLINFO i_SetScrollRange_32(HWND hwnd, int id, SCROLLINFO si, BOOL rp,
					unsigned min_pos, unsigned max_pos);

/**
	hwnd[IN] - app window handler
	id[IN] - scrollbar id
	si[IN] - SCROLLINFO struct for actions
	rp[IN] - value for request repaiting
*/
SCROLLINFO i_SetScrollPage_32(HWND hwnd, int id, SCROLLINFO si, BOOL rp,
				unsigned page_size);

/**
	hwnd[IN] - app window handler
	id[IN] - scrollbar id
	si[IN] - SCROLLINFO struct for actions
	rp[IN] - value for request repaiting
*/
SCROLLINFO i_SetScrollPos_32(HWND hwnd, int id, SCROLLINFO si, BOOL rp,
				unsigned scroll_pos);

#endif // SCROLLBAR_H_INCLUDED

#ifndef WNDPROCHANDLERS_H_INCLUDED
#define WNDPROCHANDLERS_H_INCLUDED

#include <windows.h> // for windows api

int HSCROLL_FLAG_WMHADLER;
int VSCROLL_FLAG_WMHADLER;

void wm_keydown_handler(HWND hwnd, WPARAM wParam, LPARAM lParam,
						int* xCaret, int* yCaret,
						int cxChar, int cyChar,
						int* vert_pos, int* horz_pos,
						BOOL* realloc_offset_flag,
						unsigned* offsets,
						unsigned offset_size,
						unsigned* in_offset_pos,
						//unsigned* paint_pos,
						unsigned* cur_string_num,
						unsigned screen_len_symb,
						unsigned screen_high_symb,
						unsigned buf_len_symb);


//void RecountStartIndexCaret(unsigned* offsets, unsigned offsets_size,
//					unsigned* in_offset_pos,
//					unsigned prev_off,
//					unsigned* str_num,
//					int* str_pos);
void RecountStartIndexCaret(unsigned* offsets, unsigned offsets_size,
        unsigned screen_len_symb, unsigned screen_high_symb,
        unsigned* in_offset_pos,
        unsigned prev_off,
        unsigned* str_num,
        int* str_pos, HWND hwnd,
        int* xCaret, int* yCaret,
        int* vert_pos, int* horz_pos);

#endif // WNDPROCHANDLERS_H_INCLUDED


#include "ScrollBar.h" /* module interface */

SCROLLINFO i_SetScrollRange_32(HWND hwnd, int id, SCROLLINFO si, BOOL rp,
					unsigned min_pos, unsigned max_pos)
{
    si.cbSize = sizeof(si);
    si.fMask = SIF_RANGE;
    si.nMin = min_pos;
    si.nMax = max_pos;
    SetScrollInfo(hwnd, id, &si, rp);
    return si;
}

SCROLLINFO i_SetScrollPage_32(HWND hwnd, int id, SCROLLINFO si, BOOL rp,
				unsigned page_size)
{
	si.cbSize = sizeof(si);
	si.fMask = SIF_PAGE;
	si.nPage = page_size;
	SetScrollInfo(hwnd, id, &si, rp);
	return si;
}

SCROLLINFO i_SetScrollPos_32(HWND hwnd, int id, SCROLLINFO si, BOOL rp,
				unsigned scroll_pos)
{
    si.cbSize = sizeof(si);
    si.fMask = SIF_POS;
    si.nPos = scroll_pos;
    SetScrollInfo(hwnd, id, &si, rp);
    return si;
}

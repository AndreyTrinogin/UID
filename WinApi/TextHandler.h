#ifndef TEXTHANDLER_H_INCLUDED
#define TEXTHANDLER_H_INCLUDED

/**

My string structure contains
char* str - pointer to text
unsigned len - length of this text

*/

typedef struct m_str
{
	char* str;
	unsigned len;
} str_t;

/**
	file_name[IN] - name of file for opening and reading

	buf[OUT] - str_t type, string with text from file and length of string
*/
str_t ReadFileToStrT(char* file_name);

/**
    buf[IN] - struct with string and length
    max_string_len[IN|OUT] - ptr to int var, which contains length of
    biggest string in file

    count_num_lines[OUT] - number of lines in file
*/
unsigned CalcNumLines(str_t buf, unsigned* max_string_len);

/**
	buf[IN] - struct with string and length
	num_lines[IN] - num lines in current  representation

    offsets[OUT] - ptr to unsigned int, array of offsets strings in file
*/
unsigned* CreatePointerSc(str_t buf, unsigned* num_lines);

/**
	buf[IN] - struct with string and length
	num_lines[IN] - num lines in current  representation
    max_string_len[IN] - max possible len of string, expected value cxClient/cxChar,
	it's number of symbols on screen, also expected that FONT is monospaced

    offsets[OUT] - ptr to unsigned int, array of offsets strings in file
*/
unsigned* CreatePointerWr(str_t buf, unsigned* num_lines,
						 unsigned max_string_len);

/**
    offsets[IN] - array of offsets string in file
    offsets_size[IN] - size of array
    prev_beg[IN] - previous start index for painting text

    i[OUT] - new start index for painting

    Using when changing mode or resize
*/
unsigned RecountStartIndex(unsigned* offsets, unsigned offsets_size, unsigned prev_beg);

#endif // TEXTHANDLER_H_INCLUDED


#include "TextHandler.h" // module interface
#include <stdio.h> // in-out
#include <stdlib.h> // allocations
#include <windows.h> // for type byte from winapi

str_t ReadFileToStrT(char* file_name)
{
	str_t buf = {NULL, 0};
	FILE* file = NULL;
    int err = 0;

	file = fopen(file_name, "rb");
	if(!file)
		return buf;

	err = fseek(file, 0, SEEK_END);
	if(err)
	{
		fclose(file);
		file = NULL;
		return buf;
	}

	buf.len = ftell(file);

	err = fseek(file, 0, SEEK_SET);
	if(err)
	{
		fclose(file);
		file = NULL;
		buf.len = 0;
		buf.str = NULL;
		return buf;
	}

	if (buf.len == 0)
	{
		fclose(file);
		file = NULL;
		return buf;
	}

	buf.str = (char*)malloc(sizeof(char) * (buf.len + 5));
	if(!buf.str)
	{
		fclose(file);
		file = NULL;
		buf.len = 0;
		buf.str = NULL;
		return buf;
	}

	if(fread(buf.str, sizeof(byte), buf.len, file) != buf.len)
	{
		free(buf.str);
		buf.str = NULL;
		buf.len = 0;
		fclose(file);
		file = NULL;
		return buf;
	}

	if(file != NULL)
	{
		fclose(file);
		file = NULL;
	}
    return buf;
}

unsigned CalcNumLines(str_t buf, unsigned* max_string_len)
{
	unsigned i = 0;
	unsigned count_num_lines = 0;
	unsigned count_max_string_len = 0;
	unsigned temp_max_string_len = 0;

    for(i = 0; i < buf.len; i++)
	{
		count_max_string_len++;
		if(buf.str[i] == '\n' || buf.str[i] == '\0')
		{
			if (count_max_string_len > temp_max_string_len)
				temp_max_string_len = count_max_string_len;
            count_max_string_len = 0;
            count_num_lines++;
		}
	}

	*max_string_len = temp_max_string_len;
	return count_num_lines;
}

unsigned* CreatePointerSc(str_t buf, unsigned* num_lines)
{
	unsigned* offsets = NULL; // array of offsets
	int i = 0;
	int j = 1;
	FILE* spec_info = fopen("Scroll.txt", "w");

	if(!buf.str)
		return NULL;

	offsets = (unsigned*)malloc(sizeof(int) * ((*num_lines) + 5));
	if (!offsets)
		return NULL;

	offsets[0] = 0;
	while (buf.str[i] != '\0')
	{
		if (buf.str[i] == '\n')
		{
		    fprintf(spec_info, "offsets[%d] = %d\n", j, i + 1);
			offsets[j] = i + 1;
			j++;
		}
		i++;
	}
	offsets[j] = buf.len;
	*num_lines = j;

	fclose(spec_info);
	return offsets;
}

unsigned* CreatePointerWr(str_t buf, unsigned* num_lines,
						 unsigned max_string_len)
{
	unsigned *offsets = NULL;
	unsigned i = 0;
	unsigned j = 1;
	unsigned count = 0;
	FILE *spec_info = fopen("Wrap.txt", "w");

	offsets = (unsigned*)malloc(sizeof(int) * (buf.len + 5));
	if (!offsets)
		return NULL;

	offsets[0] = 0;
	while (i <= buf.len)
	{
		if (buf.str[i] == '\n' || buf.str[i] == '\0')
		{
			offsets[j] = i + 1;
			fprintf(spec_info, "offsets[%d] = %d\n", j, i + 1);
			j++;
			count = 0;
		}
		else
            if (count == max_string_len)
            {
                offsets[j] = i - 1;
                fprintf(spec_info, "offsets[%d] = %d\n", j, i - 1);
                j++;
                count = 0;
            }
            else
                count++;
        i++;
	}
	offsets[j] = buf.len + 1;
	fclose(spec_info);
	*num_lines = j;
	return offsets;
}

unsigned RecountStartIndex(unsigned* offsets, unsigned offsets_size, unsigned prev_beg)
{
	unsigned i = 0;
    while(i < offsets_size)
    {
        //printf("offsets[%d] = %d and %d\n", i, offsets[i], prev_beg);
        if(offsets[i] == prev_beg)
            break;
        if(offsets[i + 1] > prev_beg)
            break;
        i++;
    }
    return i;
}

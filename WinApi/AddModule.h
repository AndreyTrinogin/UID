#ifndef ADDMODULE_H_INCLUDED
#define ADDMODULE_H_INCLUDED

void i_free(void** ptr);

int imax(int a, int b);

int imin(int a, int b);

#endif // ADDMODULE_H_INCLUDED
